# ms-prueba-encuesta

Este es un microservicio que permite el manejo de datos en relacion al registro de una encuesta.

## Requisitos

- Java 17
- Maven

## Construcción

Para levantar el proyecto se debe realizar como cualquier proyecto maven.
Por defecto para construir el proyecto, ejecute el siguiente comando en la raíz del proyecto:

```sh
mvn clean install
```
 
Este deberia iniciar en el puerto 8080.

EL proyecto utiliza una BD H2 por lo que no es necesario hacer nada mas.

## Endpoints

Para registrar una nueva encuesta:

[POST] localhost:8080/encuesta/

Request:

```sh
{
    "correo":"dario@correo.cl",
    "estiloMusical":"rock"
}

```
Response:

```sh
 positivo --> Status 200


 error --> 

 {
    "timestamp": "2023-09-29T12:56:28.463+00:00",
    "status": 400,
    "error": "Bad Request",
    "message": "Este correo ya contesto la encuesta anteriormente",
    "path": "/encuesta/"
} 

```

Para listar la estadistica:

[GET] localhost:8080/encuesta/estadistica

Response:

```sh
{
    "correo":"dario@correo.cl",
    "estiloMusical":"rock"
}

```

Para listar todos los estilos musicales:

[GET] localhost:8080/encuesta/estilos

Response:

```sh
[
    "Rock",
    "Jazz",
    "Pop",
    "Blues",
    "Cumbia",
    "Metal",
    "Balada",
    "Country",
    "Salsa"
]

```

localhost:8080/encuesta/estilos

## Autor.

-Darío Montiel


