package com.prueba.encuesta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.prueba.encuesta.dto.EstiloMusicalCountDTO;
import com.prueba.encuesta.entity.Encuesta;

@Repository
public interface EncuestaRepository extends JpaRepository<Encuesta, Long> {
	
	/**
	 * 
	 * @param correo
	 * @return
	 */
	Encuesta findByCorreo(String correo);
	
	/**
	 * 
	 * @return
	 */
	@Query("SELECT new com.prueba.encuesta.dto.EstiloMusicalCountDTO(COUNT(e.estiloMusical), e.estiloMusical) FROM Encuesta e GROUP BY e.estiloMusical")
	List<EstiloMusicalCountDTO> contarEstilosMusicales();
}