package com.prueba.encuesta.service;

import java.util.List;

import com.prueba.encuesta.dto.EncuestaDTO;
import com.prueba.encuesta.dto.EstiloMusicalCountDTO;
import com.prueba.encuesta.entity.Encuesta;

public interface EncuestaService {
	/**
	 * 
	 * @param encuesta
	 */
	void guardarEncuesta(EncuestaDTO encuesta);
	
	/**
	 * 
	 * @return
	 */
	public List<Encuesta> listarEncuestas(); 
	
	/**
	 * 
	 * @return
	 */
	public List<EstiloMusicalCountDTO> contarEstilosMusicales();
	
	/**
	 * 
	 * @return
	 */
	public String[] listarEstilosMusicales();
}
