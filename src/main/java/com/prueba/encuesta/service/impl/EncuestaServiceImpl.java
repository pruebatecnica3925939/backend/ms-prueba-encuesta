package com.prueba.encuesta.service.impl;

import java.util.List;
import java.util.Objects;
import static com.prueba.encuesta.util.ConstantUtil.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.prueba.encuesta.dto.EncuestaDTO;
import com.prueba.encuesta.dto.EstiloMusicalCountDTO;
import com.prueba.encuesta.entity.Encuesta;
import com.prueba.encuesta.repository.EncuestaRepository;
import com.prueba.encuesta.service.EncuestaService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class EncuestaServiceImpl implements EncuestaService {

	private final EncuestaRepository encuestaRepository;

	private static final Logger log = LoggerFactory.getLogger(EncuestaServiceImpl.class);

	public EncuestaServiceImpl(EncuestaRepository encuestaRepository) {
		super();
		this.encuestaRepository = encuestaRepository;
	}

	@Override
	public void guardarEncuesta(EncuestaDTO encuesta) {
		log.info(String.format(LOG_START, "guardarEncuesta"));

		if (Objects.nonNull(encuestaRepository.findByCorreo(encuesta.getCorreo()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ERROR_CORREO);
		}
		try {
			Encuesta nuevaEncuesta = new Encuesta();

			nuevaEncuesta.setEstiloMusical(encuesta.getEstiloMusical());
			nuevaEncuesta.setUsuario(encuesta.getCorreo());

			encuestaRepository.save(nuevaEncuesta);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ERROR_REGISTRO);
		}
		log.info(String.format(LOG_END, "guardarEncuesta()"));
	}

	@Override
	public List<Encuesta> listarEncuestas() {
		log.info(String.format(LOG_START, "listarEncuestas()"));
		return encuestaRepository.findAll();
	}

	@Override
	public List<EstiloMusicalCountDTO> contarEstilosMusicales() {
		log.info(String.format(LOG_START, "contarEstilosMusicales()"));
		return encuestaRepository.contarEstilosMusicales();

	}

	@Override
	public String[] listarEstilosMusicales() {
		log.info(String.format(LOG_START, "listarEstilosMusicales()"));
		return ESTILOS_MUSICALES;

	}

}
