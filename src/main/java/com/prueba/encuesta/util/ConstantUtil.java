package com.prueba.encuesta.util;

public class ConstantUtil {
    private ConstantUtil() {

    }
	public static final String LOG_START = "[INFO :: %s] - START.";
	public static final String LOG_END = "[INFO :: %s] - END.";
	
	public static final String ERROR_CORREO = "Este correo ya contesto la encuesta anteriormente";
	public static final String ERROR_REGISTRO= "Este correo ya contesto la encuesta anteriormente";
	
	
	public static final String[] ESTILOS_MUSICALES = { "Rock", "Jazz", "Pop", "Blues", "Cumbia", "Metal", "Balada",
			"Country", "Salsa" };
	
}
