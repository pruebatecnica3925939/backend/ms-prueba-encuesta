package com.prueba.encuesta.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.prueba.encuesta.dto.EncuestaDTO;
import com.prueba.encuesta.dto.EstiloMusicalCountDTO;
import com.prueba.encuesta.entity.Encuesta;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
public interface EncuestaController {
	
	/**
	 * 
	 * @param encuesta
	 * @return
	 */
	@ApiOperation(value = "", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful return") })
	public ResponseEntity<Void>  guardarEncuesta(EncuestaDTO encuesta);
	
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful return") })
	public ResponseEntity<List<Encuesta>> listarEncuestas();
	
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful return") })
	public ResponseEntity<List<EstiloMusicalCountDTO>> listarEstadistica();
	
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful return") })
	public ResponseEntity<String[]> listarEstilosMusicales();

}
