package com.prueba.encuesta.controller.impl;

import static com.prueba.encuesta.util.ConstantUtil.*;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.encuesta.controller.EncuestaController;
import com.prueba.encuesta.dto.EncuestaDTO;
import com.prueba.encuesta.dto.EstiloMusicalCountDTO;
import com.prueba.encuesta.entity.Encuesta;
import com.prueba.encuesta.service.EncuestaService;
import com.prueba.encuesta.service.impl.EncuestaServiceImpl;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/encuesta")
public class EncuestaControllerImpl implements EncuestaController {

	private EncuestaService encuestaService;
	private static final Logger log = LoggerFactory.getLogger(EncuestaServiceImpl.class);
	
	public EncuestaControllerImpl(EncuestaService encuestaService) {
		super();
		this.encuestaService = encuestaService;
	}

	@Override
	@PostMapping("/")
	@ApiOperation(value = "Guarda una encuesta", notes = "Guarda una encuesta")
	public ResponseEntity<Void> guardarEncuesta(@RequestBody EncuestaDTO encuesta) {
		log.info(String.format(LOG_START, "listarEstilosMusicales()"));
		encuestaService.guardarEncuesta(encuesta);
		
		return ResponseEntity.ok().build();
	}

	@Override
	@GetMapping("/")
	@ApiOperation(value = "Lista todas las encuestas", notes = "Lista todas las encuesta")
	public ResponseEntity<List<Encuesta>> listarEncuestas() {
		log.info(String.format(LOG_START, "listarEncuestas()"));
		
		List<Encuesta> response = encuestaService.listarEncuestas();
		
		log.info(String.format(LOG_END, "listarEncuestas()"));
		return ResponseEntity.ok(response);
	}
	
	@Override
	@GetMapping("/estadistica")
	@ApiOperation(value = "Lista todas las estadistica", notes = "Lista todas las estadistica")
	public ResponseEntity<List<EstiloMusicalCountDTO>> listarEstadistica() {
		log.info(String.format(LOG_START, "listarEstadistica()"));
		
		List<EstiloMusicalCountDTO> response = encuestaService.contarEstilosMusicales();
		
		log.info(String.format(LOG_END, "listarEstadistica()"));
		
		return ResponseEntity.ok(response);
	}
	
	
	@Override
	@GetMapping("/estilos")
	@ApiOperation(value = "Lista todos los estilos musicales", notes = "Lista todos los estilos musicales")
	public ResponseEntity<String[]> listarEstilosMusicales() {
		log.info(String.format(LOG_START, "listarEstilosMusicales()"));
		
		return ResponseEntity.ok(encuestaService.listarEstilosMusicales());
	}

	
	
	
}
