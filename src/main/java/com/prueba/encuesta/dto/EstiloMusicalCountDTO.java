package com.prueba.encuesta.dto;

public class EstiloMusicalCountDTO {

    private Long count;
    private String estiloMusical;

    public EstiloMusicalCountDTO(Long count, String estiloMusical) {
        this.count = count;
        this.estiloMusical = estiloMusical;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getEstiloMusical() {
        return estiloMusical;
    }

    public void setEstiloMusical(String estiloMusical) {
        this.estiloMusical = estiloMusical;
    }
}