package com.prueba.encuesta.dto;

public class EncuestaDTO {
	String correo;
	String estiloMusical;
	
	public EncuestaDTO(String correo, String estiloMusical) {
		this.correo = correo;
		this.estiloMusical = estiloMusical;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getEstiloMusical() {
		return estiloMusical;
	}

	public void setEstiloMusical(String estiloMusical) {
		this.estiloMusical = estiloMusical;
	}

	
}

