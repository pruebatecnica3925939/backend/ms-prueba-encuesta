package com.prueba.encuesta.entity;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Encuesta  implements Serializable  {
	
	private static final long serialVersionUID = 1L;
    
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String estiloMusical;
    private String correo;
	

	public Encuesta() {
		// TODO Auto-generated constructor stub
	}

	public String getEstiloMusical() {
		return estiloMusical;
	}
	public void setEstiloMusical(String estiloMusical) {
		this.estiloMusical = estiloMusical;
	}
	public String getCorreo() {
		return correo;
	}
	public void setUsuario(String correo) {
		this.correo = correo;
	}
    


  
}
