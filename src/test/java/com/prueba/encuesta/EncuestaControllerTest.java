package com.prueba.encuesta;

import static com.prueba.encuesta.util.ConstantUtil.LOG_END;
import static com.prueba.encuesta.util.ConstantUtil.LOG_START;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.prueba.encuesta.controller.impl.EncuestaControllerImpl;
import com.prueba.encuesta.dto.EncuestaDTO;
import com.prueba.encuesta.dto.EstiloMusicalCountDTO;
import com.prueba.encuesta.entity.Encuesta;
import com.prueba.encuesta.mock.EncuestaMock;
import com.prueba.encuesta.service.EncuestaService;
import com.prueba.encuesta.service.impl.EncuestaServiceImpl;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class EncuestaControllerTest {

	@InjectMocks
	private EncuestaControllerImpl controller;

	@Mock
	private EncuestaService service;

	private static final Logger log = LoggerFactory.getLogger(EncuestaServiceImpl.class);

	@Test
	void listarEncuestasTest() throws IOException {
		log.info(String.format(LOG_START, Thread.currentThread().getStackTrace()[1].getMethodName()));

		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		lenient().when(service.listarEncuestas()).thenReturn(EncuestaMock.listarEncuestas());
		ResponseEntity<List<Encuesta>> responseEntity = controller.listarEncuestas();
		assertThat(responseEntity.getStatusCode().value()).isEqualTo(200);

		log.info(String.format(LOG_END, Thread.currentThread().getStackTrace()[1].getMethodName()));
	}

	@Test
	void listarEstadisticasTest() throws IOException {
		log.info(String.format(LOG_START, Thread.currentThread().getStackTrace()[1].getMethodName()));

		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		lenient().when(service.contarEstilosMusicales()).thenReturn(EncuestaMock.listarEstadisticas());
		ResponseEntity<List<EstiloMusicalCountDTO>> responseEntity = controller.listarEstadistica();
		assertThat(responseEntity.getStatusCode().value()).isEqualTo(200);

		log.info(String.format(LOG_END, Thread.currentThread().getStackTrace()[1].getMethodName()));
	}

	@Test
	void guardarEncuestaTest() {
		log.info(String.format(LOG_START, Thread.currentThread().getStackTrace()[1].getMethodName()));
		
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		EncuestaDTO encuestaDTO = new EncuestaDTO("correo@correo.cl","Rock");
		encuestaDTO.setEstiloMusical("Rock");

		doNothing().when(service).guardarEncuesta(encuestaDTO);
		ResponseEntity<Void> responseEntity = controller.guardarEncuesta(encuestaDTO);
		verify(service).guardarEncuesta(encuestaDTO);
		assertThat(responseEntity.getStatusCode().value()).isEqualTo(200);

		log.info(String.format(LOG_END, Thread.currentThread().getStackTrace()[1].getMethodName()));
	}

	@Test
	void listarEstilosMusicalesTest() throws IOException {
		log.info(String.format(LOG_START, Thread.currentThread().getStackTrace()[1].getMethodName()));

		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		lenient().when(service.listarEstilosMusicales()).thenReturn(EncuestaMock.listarEstilosMusicales());
		ResponseEntity<String[]> responseEntity = controller.listarEstilosMusicales();
		assertThat(responseEntity.getStatusCode().value()).isEqualTo(200);

		log.info(String.format(LOG_END, Thread.currentThread().getStackTrace()[1].getMethodName()));
	}
	
}
