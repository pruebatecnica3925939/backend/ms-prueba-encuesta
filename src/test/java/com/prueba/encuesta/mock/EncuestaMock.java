package com.prueba.encuesta.mock;

import static com.prueba.encuesta.util.ConstantUtil.LOG_END;
import static com.prueba.encuesta.util.ConstantUtil.LOG_START;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.prueba.encuesta.dto.EstiloMusicalCountDTO;
import com.prueba.encuesta.entity.Encuesta;
import com.prueba.encuesta.service.impl.EncuestaServiceImpl;

public class EncuestaMock {
	
	private static final Logger log = LoggerFactory.getLogger(EncuestaServiceImpl.class);
	
	public static List<Encuesta> listarEncuestas() {
		log.info(String.format(LOG_START, Thread.currentThread().getStackTrace()[1].getMethodName()));

		Encuesta encuesta = new Encuesta();
		encuesta.setEstiloMusical("rock");
		encuesta.setUsuario("user@correo.cl");
		
		List<Encuesta> encuestas = new ArrayList<>();
		encuestas.add(encuesta);
		

		log.info(String.format(LOG_END, Thread.currentThread().getStackTrace()[1].getMethodName()));
		return encuestas;
	}
	
	public static List<EstiloMusicalCountDTO> listarEstadisticas() {
		log.info(String.format(LOG_START, Thread.currentThread().getStackTrace()[1].getMethodName()));

		EstiloMusicalCountDTO estilo = new EstiloMusicalCountDTO(Long.valueOf(23L),"Rock");
		
		List<EstiloMusicalCountDTO> estilos = new ArrayList<>();
		estilos.add(estilo);
		
		log.info(String.format(LOG_END, Thread.currentThread().getStackTrace()[1].getMethodName()));
		return estilos;
	}
	
	public static String[] listarEstilosMusicales() {
		log.info(String.format(LOG_START, Thread.currentThread().getStackTrace()[1].getMethodName()));

		String[] estilos = {"rock", "salsa","blues"}; 
		
		
		log.info(String.format(LOG_END, Thread.currentThread().getStackTrace()[1].getMethodName()));
		return estilos;
	}
	
	
	
	
	
	
}
